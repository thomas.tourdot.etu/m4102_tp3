## URI
/pizzas					GET			liste des pizzas
/pizzas/{id}			GET			une pizza
/pizzas/{id}/name		GET			le nom de la pizza
/pizzas					POST		Nouvelle pizza
/pizzas/{id}			DELETE		

## Méthodes

## Représentations